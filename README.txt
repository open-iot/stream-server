# How to install, as following
``` bash
# http://open.tendi.cn/libs/tendi/tlive/root.zip

PROJECT_DIR=$PWD
unalias cp

# stream server
cd $PROJECT_DIR/pkg
rm -rf root.zip root
cat root.zip.a* > root.zip
unzip root.zip
cp root/* / -rf
rm -rf root.zip root
cat root.zip.a* > root.zip
chmod 777 /opt/live -R

systemctl enable live
systemctl restart live

# stream api service
cd $PROJECT_DIR/src
npm install
pm2 start app.js --name stream-server-api

pm2 startup
pm2 save --force
cd $PROJECT_DIR
```

# live web console
http://live.tendic.cn/console/ng_index.html#/connect
47.101.70.189:1937

# srs rtmp player
http://ossrs.net/players/srs_player.html?vhost=__defaultVhost__&app=28181&stream=34020000001320000002@34020000001320000002&server=47.101.70.189&port=1935&autostart=true#

# srs gb28281 tab
http://ossrs.net/players/srs_gb28181.html

# 查询
sip会话查询： http://47.101.70.189:1937/api/v1/gb28181?action=sip_query_session
GB28181媒体通道查询： http://47.101.70.189:1937/api/v1/gb28181?action=query_channel

# Ancient-Live code
https://code.tendi.cn/sugar-inc/Ancient-Live

# streams
http://47.101.70.189:1936/28181/34020000001320000002@34020000001320000002.flv
rtmp://47.101.70.189:1935/28181/34020000001320000002@34020000001320000002

# delete large file,https://gitee.com/help/articles/4232#article-header0
```
git filter-branch --tree-filter 'rm -f path/to/large/files' --tag-name-filter cat -- --all
git push origin --tags --force
git push origin --all --force
```
