
const path = require('path');
const fs = require('fs');
const Koa = require('koa');
const Router = require('koa-router');
const koaStatic = require('koa-static');
const bodyparser = require('koa-bodyparser');

const app = new Koa();

app.use(bodyparser());

const router = new Router();

var basedir = "/Ancient/1/live";

// static
app.use(koaStatic(__dirname + '/public'));
app.use(koaStatic(basedir));

// middleware
app.use((ctx, next) => {
    ctx.params = {
        ...ctx.query,
        ...ctx.request.body
    }

    next();
});

// cross domin
app.use((ctx, next) => {
    // allow cross
    ctx.set("Access-Control-Allow-Origin", "*");
    ctx.set("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    // request header
    ctx.set(
        "Access-Control-Allow-Headers",
        `Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild,x-token,sessionToken,token`
    );
    if (ctx.method == "OPTIONS") {
        ctx.body = 200;
    } else {
        next();
    }
})

// views
const views = require("koa-views");
app.use(views(path.join(__dirname, "views"), {
    map: {
        html: 'underscore'
    }
}));

router.get("/tpl", async ctx => {
    await ctx.render("tpl", {
        title: "Grayly"
    })
})

// api
router.all('/get_record_list', ctx => {

    /**
     * walkDirSync
     */
    function walkDirSync(dir) {
        var fileList = new Array();

        var readDir = fs.readdirSync(dir);
        readDir.forEach((value) => {
            var fullPath = path.join(dir, value);
            var stats = fs.statSync(fullPath);
            if (stats.isFile()) {
                fileList.push(fullPath);
            } else if (stats.isDirectory()) {
                fileList = fileList.concat(walkDirSync(fullPath));
            }
        })

        return fileList;
    }

    ctx.set("Content-Type", "application/json")

    try {
        const { id, device_id, year, month, day } = ctx.request.body;

        var finalList = new Array();
        if (!id || !device_id || !year || !month || !day) {
            console.log(`body:${JSON.stringify(ctx.request.body)}. id, device_id, year, month, day was required, please check params`);
        } else {
            var dir = `${basedir}/28181/${id}@${device_id}/${year}/${month}/${day}`;
            var fileList = walkDirSync(dir);

            var basedirLen = basedir.length;
            fileList.forEach((file) => {
                if ((/\.flv$|\.mp4$/i).test(file)) {
                    finalList.push(file.substring(basedirLen))
                }
            });
        }

        ctx.body = JSON.stringify({
            data: finalList
        });
    } catch (err) {
        console.log(`get_record_list exception: ${err}`);
        ctx.body = JSON.stringify({ message: `${err}` });
    }
})

app.use(router.routes());

// listen
var port = process.env.PORT ? process.env.PORT : 9801;
app.listen(port, () => {
    console.log(`http://$host:${port}`);
});
